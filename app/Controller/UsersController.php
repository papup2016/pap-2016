	<?php
	App::uses('AppController', 'Controller');
	class UsersController extends AppController {
		public function index(){
				if ($this->request->is('post')){
					$userData = $this->request->data;
					$users = $this->User->find('all',array('conditions'=>array('User.nome'=>$userData['User']['nome'])));
					$this->set('users', $users);
				}
		}

		public function edit(){
			if($this->Session->read('User')){
				$user = $this->User->findById($this->Session->read('User.id'));
				debug($user);die;
				if ($this->request->data){
					$retorno['senha'] = '';
					$userData = $this->request->data;
					unset($userData['email']);
					if($userData['senha']!=''){
						if($userData['senha_nova_confirmacao']==$userData['senha_nova']&& strlen($userData['senha_nova']>=8)){
							if($this->encodePass($userData['senha']) === $user['senha']){
								unset($userData['senha_nova_confirmacao']);
								unset($userData['senha_nova']);
								$user['User']['senha'] = $this->encodePass($userData['senha_nova']);
								if( $this->User->save($user)){
									$retorno['info'] = "Editado com sucesso";
									// unset();
								}
							}else{
								$retorno['senha'] = 'Senha antiga incorreta!';
							}
						}else {
							$retorno['senha'] = 'As senhas não são iguais ou não contém mais de 8 caracateres';
						}
					}else {
						$retorno['senha'] = 'Senha antiga incorreta!';
					}

					$this->layout = false;
					$this->render(false);
					$this->set('_serialize', 'data');
					echo json_encode($retorno);
				}
				$this->data = $user;
				$user = $this->User->removerDesnecessarios($user);
			}else{
				$this->redirect(array('controller' => 'Home', 'action' => 'index'));
			}
		}
		public function logout(){

			if($this->Session->read('User')){
				$this->Session->write('User', null);
				$retorno['sucesso'] = "Volte logo!";
			}else {
				$retorno['erro'] = "Você precisa estar conectado";
			}
			$this->layout = false;
			$this->render(false);
			$this->set('_serialize', 'data');
			echo json_encode($retorno);
			$this->redirect(array('controller' => 'Home', 'action' => 'index'));
		}
		public function login(){
			if(!$this->Session->read('User')){
				if ($this->request->is('post')){
					$login = $this->request->data;
					if($login){
						$user = $this->User->find('first', array(
						'conditions'=>array('User.email' => $login['User']["email"],'User.senha' => $this->encodePass($login['User']["senha"]))));
						if($user){
							$user = $this->User->removerDesnecessarios($user);
							$this->Session->write($user);
							$retorno['sucesso'] = "Seja bem vindo!";
						}else {
							$retorno['erro'] = "Senha ou email inválido";
						}
					}
				}
			}else {
				$retorno['erro'] = "Já logado";
			}
			$this->layout = false;
			$this->render(false);
			$this->set('_serialize', 'data');
			echo json_encode($retorno);
		}

		public function page(){
			if($this->Session->read('User')){
				$usuario = $this->Session->read('User');
				$user = $this->User->findById($usuario['id']);
				if($user){
					$this->set('user', $user["User"]);

				}else {
					$this->redirect(array('controller' => 'Home', 'action' => 'index'));
				}
			}else {
				$this->redirect(array('controller' => 'Home', 'action' => 'index'));
			}
		}
		public function create(){
			// debug($this->request->data);die;
			if(!$this->Session->read('user')){
				if ($this->request->is('post')){
				$usuario = $this->request->data;
				$retorno = array();
				if($usuario['User']['senha']==$usuario['User']['senha_confirmacao'] && strlen($usuario['User']['senha'])>=8 ){
						$conditions = array('User.email' => $usuario['User']['email']);
						if (!$this->User->hasAny($conditions)){
							unset($usuario['User']['senha_confirmacao']);
							//save new user
							$usuario['User']['senha'] = $this->encodePass($usuario['User']['senha']);
							if ($this->User->save($usuario)){
								$retorno['sucesso']='Cadastro realizado com sucesso.';
							}else{
								$retorno['erro']='Falha ao cadastrar';
							}
						}else {
							$retorno['erro']='Você já é cadastrado';
						}


					}else {
						$retorno['erro']='Usuário e/ou senha inválido.';
					}
				}
				$this->layout = false;
				$this->render(false);
				$this->set('_serialize', 'data');
				echo json_encode($retorno);
		}else {
			$this->redirect(array('controller' => 'Home', 'action' => 'index'));
		}
	}
}
