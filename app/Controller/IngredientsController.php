	<?php
	App::uses('AppController', 'Controller');
	class IngredientsController extends AppController {

		public function index(){}

		public function add(){
				if ($this->request->is('post')){
					$data = $this->request->data;
					if($this->Ingredient->find('first', array('conditions' => array('Ingredient.nome' => $data['Ingredient']['nome'])))){
						$this->Session->setFlash('Esse ingrediente já existe!', 'flash_danger');
					}else{
						$this->Ingredient->save($data);
						$this->Session->setFlash('Ingrediente cadastrado com sucesso!', 'flash_success');
					}
				}
				$this->data = "";
				$this->loadModel('IngredientCategory');
				$categorias = $this->IngredientCategory->find('list', array('fields' => array('IngredientCategory.id', 'IngredientCategory.nome')));
				$this->set('categorias', $categorias);
		}

		public function getForRecipe($var = null){
			if($var === 'R3c1p3F1nd3R13asd13asd'){
				$all = $this->Ingredient->find('all', array('order' => 'Ingredient.nome asc'));
				echo json_encode($all);die;
			}else{
				$this->redirect(array(
            'controller'=>'Home',
            'action' => 'index'
        ));
			}
		}
}
