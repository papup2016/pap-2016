<?php

App::uses('Controller', 'Controller');
class AppController extends Controller {
  public function encodePass($pass){
    $pass = base64_encode(base64_encode($pass));
    return $pass;
  }
  public function decodePass($pass){
    $pass = base64_decode(base64_decode($pass));
    return $pass;
  }
}
