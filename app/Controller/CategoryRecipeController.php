	<?php
	App::uses('AppController', 'Controller');
	class CategoryRecipeController extends AppController {
		public function index(){
			if($this->Session->read('User')){
				$CategoryRecipe = $this->CategoryRecipe->find('all');
				$this->set('CategoryRecipe', $CategoryRecipe);
			}
		}
		public function edit(){
			if($this->Session->read('User')){
				$id = (int) $this->request->params["pass"]["0"];
				if($id){
					$CategoryRecipe = $this->CategoryRecipe->find('all', array(
						'conditions'=>array('CategoryRecipe.id =' => $id)));
						if($CategoryRecipe){
							$this->data = $CategoryRecipe[0]["CategoryRecipe"];
						}else {
							$this->redirect(array('controller' => 'Home', 'action' => 'index'));
						}
					}
			}else {
				$this->redirect(array('controller' => 'Home', 'action' => 'index'));
			}
		}
		public function create(){
			if($this->Session->read('User')){
				if ($this->request->is('post')){
				$categoria = $this->request->data;
				// debug($categoria);die;
				$retorno = array();
				if($categoria['CategoryRecipe']['nome'] && $categoria['CategoryRecipe']['status']){
						$conditions = array('CategoryRecipe.nome' => $categoria['CategoryRecipe']['nome']);
						if (!$this->CategoryRecipe->hasAny($conditions)){
							if ($this->CategoryRecipe->save($categoria)){
								$retorno['sucesso']='Cadastro realizado com sucesso.';
							}else{
								$retorno['erro']='Falha ao cadastrar';
							}
						}else {
							$retorno['erro']='Já cadastrado';
						}


					}else {
						$retorno['erro']='Não preenchido';
					}
					$this->layout = false;
					$this->render(false);
					$this->set('_serialize', 'data');
					echo json_encode($retorno);
				}
		}else {
			$this->redirect(array('controller' => 'Home', 'action' => 'index'));
		}
	}
}
