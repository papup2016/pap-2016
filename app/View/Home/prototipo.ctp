<html><head>
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">		<title>
			Recipe Finder: O lugar certo para você procurar a receita perfeita!					</title>
		<link href="/pap/favicon.ico" type="image/x-icon" rel="icon"><link href="/pap/favicon.ico" type="image/x-icon" rel="shortcut icon"><link rel="stylesheet" type="text/css" href="/pap/tema/css/bootstrap.min.css"><link rel="stylesheet" type="text/css" href="/pap/tema/font-awesome/4.3.0/css/font-awesome.min.css"><link rel="stylesheet" type="text/css" href="/pap/tema/css/icon.css"><link rel="stylesheet" type="text/css" href="/pap/tema/css/loader.css"><link rel="stylesheet" type="text/css" href="/pap/tema/css/idangerous.swiper.css"><link rel="stylesheet" type="text/css" href="/pap/tema/css/jquery-ui.css"><link rel="stylesheet" type="text/css" href="/pap/tema/css/stylesheet.css"><script type="text/javascript" src="/pap/tema/js/jquery-2.1.4.min.js"></script><style type="text/css"></style><script type="text/javascript" src="/pap/tema/js/jquery-ui.min.js"></script><script type="text/javascript" src="/pap/tema/js/jquery.mixitup.js"></script><script type="text/javascript" src="/pap/tema/js/jquery.viewportchecker.min.js"></script>	</head>
	<body class="">
		<div class="be-loader" style="display: none;">
			<div class="spinner">
				<p class="circle">
					<span class="ouro">
						<span class="left"><span class="anim"></span></span>
						<span class="right"><span class="anim"></span></span>
					</span>
				</p>
			</div>
		</div>
		<header>

			<div class="container-fluid custom-container">
				<div class="row no_row row-header">
					<div class="brand-be">
						<a href="/pap/"><h2><span class="label label-default">R.</span></h2></a>						<!-- <a href="/">
							<h2><span class="label label-default">R.</span></h2>
						</a> -->
					</div>
					<div class="header-menu-block">
						<button class="cmn-toggle-switch cmn-toggle-switch__htx"><span></span></button>
						<ul class="header-menu" id="one" style="max-height: 636px;">
							<li>
								<a href="/pap/">Nova Pesquisa</a>							</li>
							<li><a href="#">Usuários</a>
								<ul>
									<li>
											<a href="/pap/Usuarios">Listar</a>
									</li>
									<li>
											<a href="/pap/Usuarios/cadastrar">Cadastrar</a>
									</li>
								</ul>
							</li>
							<li><a href="#">Ingredientes / Receitas</a>
								<ul>
									<li>
											<a href="/pap/Ingredientes/cadastrar">Cadastrar Ingrediente</a>
									</li>
									<li>
											<a href="/pap/Ingredientes">Input Ingredientes Banco</a>
									</li>
									<li>
											<a href="/pap/Ingredientes/IngredientesReceitas/R3c1p3F1nd3R13asd13asd">Get Ingredient For Recipe</a>
									</li>
								</ul>
							</li>
							<li><a href="#">Olá mundo</a>
								<ul>
									<li><a href="search.html">Explore</a></li>
									<li><a href="people.html">People</a></li>
									<li><a href="gallery.html">Galleries</a></li>
								</ul>
							</li>
						</ul>
					</div>
																<div class="login-header-block">
							<div class="login_block">
								<a class="btn-login btn color-1 size-2 hover-2" href="#"><i class="fa fa-user"></i>
								Log in</a>
							</div>
						</div>
					</div>
								</div>

		</header>

		<div id="content-block">

				</div>

		<div class="large-popup login">
			<div class="large-popup-fixed"></div>
			<div class="container large-popup-container">
				<div class="row">
					<div class="col-md-8 col-md-push-2 col-lg-6 col-lg-push-3  large-popup-content">
						<div class="row">
							<div class="col-md-12">
								<i class="fa fa-times close-button"></i>
								<h5 class="large-popup-title">Log in</h5>
							</div>
							<div class="col-md-12">
								<h2 id="resposta_login"></h2>
							</div>
							<form action="" id="login_usuario" type="POST" class="popup-input-search">
							<div class="col-md-6">
								<input class="input-signtype" name="email" type="email" required="" placeholder="Seu email">
							</div>
							<div class="col-md-6">
								<input class="input-signtype" name="senha" type="password" required="" placeholder="Senha">
							</div>
							<div class="col-xs-6">

								<a href="/users/recover" class="link-large-popup">Recuperar Senha?</a>
							</div>
							<div class="col-xs-6 for-signin">
								<input type="submit" class="be-popup-sign-button" value="SIGN IN">
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="large-popup register">
			<div class="large-popup-fixed"></div>
			<div class="container large-popup-container">
				<div class="row">
					<div class="col-md-10 col-md-push-1 col-lg-8 col-lg-push-2 large-popup-content">
						<div class="row">
							<div class="col-md-12">
								<i class="fa fa-times close-button"></i>
								<h5 class="large-popup-title">Register</h5>
							</div>
							<form action="/users/create" class="popup-input-search" id="cadastro_usuario">
								<div class="col-md-6">
									<input class="input-signtype" type="text" required="" placeholder="Nome" name="nome">
								</div>
								<div class="col-md-6">
									<input class="input-signtype" type="email" required="" placeholder="Email" name="email">
								</div>
								<div class="col-md-6">
									<input class="input-signtype" type="password" required="" placeholder="Senha" name="senha">
								</div>
								<div class="col-md-6">
									<input class="input-signtype" type="password" required="" placeholder="Repita sua senha" name="senha_confirmacao">
								</div>
								<div class="col-md-6 for-signin">
									<input type="submit" class="be-popup-sign-button" value="CADASTRAR">
								</div>
							</form>
							<div><p id="resposta_cadastro"></p></div>
						</div>
					</div>
				</div>
			</div>
		</div>


			<div id="container" style="
    padding-top: 50px;
">
				<div id="content">


					<div class="container-fluid custom-container">
  <div class="row">
    <div class="col-md-2 left-feild">
      <form class="input-search">
        <input type="text" required="" placeholder="Procurar por ingredientes">
          <i class="fa fa-search"></i>
          <input type="submit" value="">
      </form>
    </div><div class="col-md-2 left-feild">
      <form class="input-search">
        <input type="text" required="" placeholder="Procurar por receitas">
          <i class="fa fa-search"></i>
          <input type="submit" value="">
      </form>
    </div>
    <div class="col-md-2 left-feild">
      <div class="for-be-dropdowns">
				<div class="be-drop-down" style="width:100%">
          <i class="icon-projects"></i>
          <span class="be-dropdown-content">Categorias</span>
          <ul class="drop-down-list">
            <li class="filter" data-filter=".category-4"><a>Doces</a></li>
            <li class="filter" data-filter=".category-5"><a>Salgadas</a></li>
            <li class="filter" data-filter=".category-1"><a>Veganas</a></li>
						<li class="filter" data-filter=".category-1"><a>Saudáveis</a></li>
          </ul>
        </div>
      </div>
    </div>
		<div class="col-md-2 left-feild">
      <div class="for-be-dropdowns">
				<div class="be-drop-down"  style="width:100%">
          <i class="icon-projects"></i>
          <span class="be-dropdown-content">Por avaliação</span>
          <ul class="drop-down-list">
            <li class="filter" data-filter=".category-4"><a>Melhor Avaliado</a></li>
            <li class="filter" data-filter=".category-5"><a>Mais Avaliado</a></li>
            <li class="filter" data-filter=".category-1"><a>Pior Avaliado</a></li>
						<li class="filter" data-filter=".category-1"><a>Menos Avaliado</a></li>
          </ul>
        </div>
      </div>
    </div>
		<div class="col-md-2 left-feild">
      <div class="for-be-dropdowns">
				<div class="be-drop-down"  style="width:100%">
          <i class="icon-projects"></i>
          <span class="be-dropdown-content">Por tempo</span>
          <ul class="drop-down-list">
						<li class="filter" data-filter=".category-1"><a>Nenhum</a></li>
            <li class="filter" data-filter=".category-4"><a>Mais demorado</a></li>
            <li class="filter" data-filter=".category-5"><a>Mais rápido</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>
<div class="s_keywords">
  <div class="container-fluid custom-container">
    <a class="btn color-1 size-3 hover-10"><i class="fa fa-trash-o"></i>Limpar Filtros</a>
    <a class="btn color-6 size-3 hover-10">Cebola<i class="fa keyword fa-times"></i></a>
    <a class="btn color-6 size-3 hover-10">Sal<i class="fa keyword fa-times"></i></a>
    <a class="btn color-6 size-3 hover-10">Pimentão<i class="fa keyword fa-times"></i></a>


  </div>
</div>
<div class="container-fluid custom-container">
  <div class="row">

    <div class="col-md-2 left-feild">

      <div class="be-vidget">
        <h3 class="letf-menu-article">
          Ingredientes Populares
        </h3>
        <div class="tags_block clearfix">
          <ul>
            <li><a data-filter=".category-6" class="filter">Sal</a></li>
            <li><a data-filter=".category-1" class="filter">Chocolate</a></li>
            <li><a data-filter=".category-2" class="filter">Azeite</a></li>
            <li><a data-filter=".category-3" class="filter">Carne</a></li>
            <li><a data-filter=".category-4" class="filter">Frango</a></li>
            <li><a data-filter=".category-5" class="filter">Arroz</a></li>
            <li><a data-filter=".category-6" class="filter">Cebola</a></li>



          </ul>
        </div>
      </div>

    </div>

    <div class="col-md-10">
      <div id="container-mix" class="row _post-container_ fail">
        <div class="gallery-box clearfix">
          <a class="gallery-a" href="#"><img src="http://4.bp.blogspot.com/-W2thp1CTmk0/UZ3IeB0rPoI/AAAAAAAAMFU/KpVn9HqSXk8/s1600/2,+PIZZA,+PIZZA+PICANTE,+RECETAS+DE+COCINA.jpg" style="max-width:180px" alt="img"></a>
          <div class="gallery-info">
            <h3><a href="/users/page/">Pizza Picante</a></h3>
          </div>
          <div class="gallery-btn">
            <a class="btn-login btn color-2 size-2 hover-2" href="#">Ver</a>

          </div>
        </div>
        <div class="gallery-box clearfix">
          <a class="gallery-a" href="#"><img src="http://cptstatic.s3.amazonaws.com/imagens/enviadas/materias/materia8336/bacalhoada-vegetariana-cursos-cpt.jpg" style="max-width:180px" alt="img"></a>
          <div class="gallery-info">
            <h3><a href="/users/page/">Bacalhoada</a></h3>
          </div>
          <div class="gallery-btn">
            <a class="btn-login btn color-2 size-2 hover-2" href="#">
              Ver
              </a>

          </div>
        </div>
      </div>
    </div>
				</div>
				<div id="footer">
										<p>
											</p>
				</div>
			</div>
				<div class="theme-config">
		    <div class="main-color">
		        <div class="title">Main Color:</div>
		        <div class="colours-wrapper">
		            <div class="entry color1 m-color active" data-colour="/tema/css/stylesheet.css"></div>
		            <div class="entry color3 m-color" data-colour="/tema/css/style-green.html"></div>
		            <div class="entry color6 m-color" data-colour="/tema/css/style-orange.html"></div>
		            <div class="entry color8 m-color" data-colour="/tema/css/style-red.html"></div>
		            <div class="title">Second Color:</div>
		            <div class="entry s-color  active color10" data-colour="/tema/css/stylesheet.css"></div>
		            <div class="entry s-color color11" data-colour="/tema/css/style-oranges.html"></div>
		            <div class="entry s-color color12" data-colour="/tema/css/style-greens.html"></div>
		            <div class="entry s-color color13" data-colour="/tema/css/style-reds.html"></div>
		        </div>
		    </div>
		   <div class="open"><img src="/pap/imagens/icon-134.png" alt=""></div>
		</div>
