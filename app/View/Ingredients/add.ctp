<?= $this->Form->create('Ingredient', array('type' => 'post')); ?>
<div class="row">
				<div class="col-xs-12 col-md-12 _editor-content_">
					<div class="sec"  data-sec="basic-information">
						<div class="be-large-post">
							<div class="info-block style-2">
								<div class="be-large-post-align "><h3 class="info-block-label">Cadastro de Ingrediente</h3></div>
							</div>
							<div class="be-large-post-align">
								<div class="row">
									<h3 id="dados_mensagem"></h3>
									<div class="input-col col-xs-12 col-sm-6">
										<div class="form-group fg_icon focus-2">
											<div class="form-label">Nome do Ingrediente</div>
											<?= $this->Form->input('nome', array('label' => false, 'class' => 'form-input')); ?>
											<!-- <input class="form-input" value="<?php echo $user["nome"] ?>" name="nome" type="text"> -->
										</div>
                    <div class="form-group fg_icon focus-2">
											<div class="form-label">Categoria</div>
											<?= $this->Form->input('categoria_ingrediente_id', array('type' => 'select', 'label' => false, 'options' => $categorias, 'empty' => 'Selecione uma categoria', 'selected' => 'Selecione uma categoria', 'class' => 'be-drop-down')); ?>
											<!-- <input class="form-input" value="<?php echo $user["email"] ?>" type="text" name="email" disabled value=""> -->
										</div>
										<div class="col-xs-12">
											<?= $this->Form->submit('Salvar', array('class' => 'btn color-1 size-4 hover-1 btn-right')) ?>
											<!-- <button class="btn color-1 size-4 hover-1 btn-right" type="submit">Salvar</button> -->
											<input type="button" value="Voltar" onClick="history.go(-1);return true;" class="btn color-2 size-4 hover-1 btn-right">
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript">
		$('#edit_user').submit(function(event){
			event.preventDefault();
			dados = new FormData(this);
			$.ajax({
				type: 'POST',
				url: "/users/edit",
				data: dados,
				processData: false,
				cache: false,
				contentType: false,
				dataType: "json",
				success: function(json) {
					if('info' in json) {
						$("#dados_mensagem").html(json.info);
						$("#dados_mensagem").removeClass("text-danger");
						$("#dados_mensagem").addClass("text-success");
						//setTimeout(function(){location.reload()},1000);
					}
					if('senha' in json) {
						$("#senha_mensagem").html(json.senha);
						$("#senha").val('');
						$("#senha_nova").val('');
						$("#senha_nova_confirmacao").val('');
						$("#senha_mensagem").addClass("text-success");

					}
				}
			});
		});
		</script>
