<p class="alert alert-dismissible alert-success">
  <button type="button" class="close" data-dismiss="alert">X</button>
  <?php echo $message; ?>
</p>
