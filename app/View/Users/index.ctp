  <br/>
  <div class="custom-container gallery-container">
    <div class="container">
      <?php echo $this->Form->create('User'); ?>
      <!-- <form class="" action="/users" method="post"> -->
        <div class="row">
          <div class="col-md-6">
            <div class="form-group fg_icon focus-1">
              <?php echo $this->Form->input('nome', array('class' => 'form-input', 'placeholder' => 'Nome do usuário', 'label' => false)) ?>
              <!-- <input type="text" class="form-input" name="busca" placeholder="Nome do usuário"> -->
            </div>
          </div>
          <button class="btn btn-default navbar-btn" type="submit"><i class="fa fa-search"></i>Buscar</button>
        </div>
      <!-- </form> -->
      <?php echo $this->Form->end(); ?>
  <?php if(isset($users)){
  foreach ($users as $user) : ?>
      <div class="gallery-box clearfix">
        <a class="gallery-a" href="#"><img src="img/gallery-img-1.png" alt="img"></a>
        <div class="gallery-info">
          <h3>
            <?php echo $this->Html->link($user['User']['nome'], array('controller' => 'Usuarios', 'action' => 'visualizar')); ?>
            <!-- <a href="/users/page/<?php echo $value['id'] ?>"><?php echo $value['nome'] ?></a> -->
          </h3>
        </div>
        <div class="gallery-btn">
          <a class="btn-login btn color-1 size-2 hover-2" href="#">
            FOLLOW
            </a>
          <a class="btn-login btn color-1 size-2 hover-2" href="#">
            JOIN
          </a>
        </div>
      </div>
  <?php endforeach;} ?>
  </div>
</div>
</div>
