<?= $this->Form->create('User'); ?>
<?= $this->Form->hidden('id'); ?>
<!-- <form action="/users/edit" method="post" id="edit_user"> -->
	<!-- <input class="form-input" hidden value="<?php echo $user["id"] ?>" name="id" type="text"> -->
<div class="row">
				<div class="col-xs-12 col-md-12 _editor-content_">
					<div class="sec"  data-sec="basic-information">
						<div class="be-large-post">
							<div class="info-block style-2">
								<div class="be-large-post-align "><h3 class="info-block-label">Informações Básicas</h3></div>
							</div>
							<div class="be-large-post-align">
								<div class="row">
									<h3 id="dados_mensagem"></h3>
									<div class="input-col col-xs-12 col-sm-6">
										<div class="form-group fg_icon focus-2">
											<div class="form-label">Nome</div>
											<?= $this->Form->input('nome', array('class' => 'form-input', 'label' => false)); ?>
											<!-- <input class="form-input" value="<?php echo $user["nome"] ?>" name="nome" type="text"> -->
										</div>
                    <div class="form-group fg_icon focus-2">
											<div class="form-label">E-mail</div>
											<?= $this->Form->input('email', array('class' => 'form-input', 'type' => 'email', 'label' => false, 'disabled')); ?>
											<!-- <input class="form-input" value="<?php echo $user["email"] ?>" type="text" name="email" disabled value=""> -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="sec"  data-sec="edit-password">
						<div class="be-large-post">
							<div class="info-block style-1">
								<div class="be-large-post-align"><h3 class="info-block-label">Alterar senha</h3></div>
							</div>
							<div class="be-large-post-align">
								<div class="row">
									<h3 id="senha_mensagem"></h3>
									<div class="input-col col-xs-12 col-sm-4">
										<div class="form-group focus-2">
											<div class="form-label">Senha antiga</div>
											<?= $this->Form->input('senha', array('class' => 'form-input', 'type' => 'password', 'id' => 'senha')); ?>
											<!-- <input class="form-input" type="password" name="senha" id="senha" name placeholder=""> -->
										</div>
									</div>
									<div class="input-col col-xs-12 col-sm-4">
										<div class="form-group focus-2">
											<div class="form-label">Nova senha</div>
											<?= $this->Form->input('senha_nova', array('class' => 'form-input', 'type' => 'password', 'id' => 'senha_nova')); ?>
											<!-- <input class="form-input" type="password" name="senha_nova" id="senha_nova" placeholder=""> -->
										</div>
									</div>
									<div class="input-col col-xs-12 col-sm-4">
										<div class="form-group focus-2">
											<div class="form-label">Repetir nova senha</div>
											<?= $this->Form->input('senha_nova_confirmacao', array('class' => 'form-input', 'type' => 'password', 'id' => 'senha_nova_confirmacao')); ?>
											<!-- <input class="form-input" type="password" id="senha_nova_confirmacao" name="senha_nova_confirmacao" placeholder=""> -->
										</div>
									</div>
								</div>
								<div class="col-xs-12">
									<a class="btn color-2 size-2 hover-1 btn-right" href="/">Cancelar</a>
									<button class="btn color-1 size-2 hover-1 btn-right" type="submit">Salvar</button>
								</div>
								<?= $this->Form->end(); ?>
							</div>
						</div>
					</div>
				</div>
			</div>
		</form>
		<script type="text/javascript">
		$('#edit_user').submit(function(event){
			event.preventDefault();
			dados = new FormData(this);
			$.ajax({
				type: 'POST',
				url: "/users/edit",
				data: dados,
				processData: false,
				cache: false,
				contentType: false,
				dataType: "json",
				success: function(json) {
					if('info' in json) {
						$("#dados_mensagem").html(json.info);
						$("#dados_mensagem").removeClass("text-danger");
						$("#dados_mensagem").addClass("text-success");
						//setTimeout(function(){location.reload()},1000);
					}
					if('senha' in json) {
						$("#senha_mensagem").html(json.senha);
						$("#senha").val('');
						$("#senha_nova").val('');
						$("#senha_nova_confirmacao").val('');
						$("#senha_mensagem").addClass("text-success");

					}
				}
			});
		});
		</script>
