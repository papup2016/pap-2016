<!-- MAIN CONTENT -->
<div id="content-block">
	<div class="container be-detail-container">
		<div class="row">
			<div class="col-xs-12 col-md-4 left-feild">
				<div class="be-user-block style-3">
					<div class="be-user-detail">
						<a class="be-ava-user style-2" href="blog-detail-2.html">
							<img src="img/avatar.png" alt="">
						</a>
						<!--<a class="be-ava-left btn color-1 size-2 hover-1"><i class="fa fa-plus"></i>Follow</a-->
						<p class="be-use-name"><?php echo $user["nome"] ?></p>
						<a class="badge" href="/users/edit">Minhas informações</a>
					</div>
					<div class="be-user-statistic">
						<div class="stat-row clearfix"><i class="stat-icon icon-views-b"></i> Receitas<span class="stat-counter">218098</span></div>
						<div class="stat-row clearfix"><i class="stat-icon icon-like-b"></i>Recietas Favoritas<span class="stat-counter">14335</span></div>
						<div class="stat-row clearfix"><i class="stat-icon icon-followers-b"></i>Followers<span class="stat-counter">2208</span></div>
						<div class="stat-row clearfix"><i class="stat-icon icon-following-b"></i>Following<span class="stat-counter">0</span></div>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-8">
									<div class="tab-wrapper style-1">
											<div class="tab-nav-wrapper">
													<div  class="nav-tab  clearfix">
															<div class="nav-tab-item active">
																	<span>Minhas Receitas</span>
															</div>
															<div class="nav-tab-item ">
																	<span>Receitas Favoritas</span>
															</div>
													</div>
											</div>
											<div class="tabs-content clearfix">
													<div class="tab-info active">
                            <div class="gallery-box clearfix">
                              <a class="gallery-a" href="#"><img src="img/gallery-img-1.png" alt="img"></a>
                              <div class="gallery-info">
                                <h3><a href="/users/page/">Pizza</a></h3>
                              </div>
                              <div class="gallery-btn">
                                <a class="btn-login btn color-2 size-2 hover-2" href="#">
                                  Ver
                                  </a>
                                <a class="btn-login btn color-1 size-2 hover-2" href="#">
                                  Salvar
                                </a>
                              </div>
                            </div>
													</div>
													<div class="tab-info">
													</div>
											</div>
									</div>
			</div>
		</div>
	</div>
</div>
