	<?php

	// $cakeDescription = __d('cake_dev', 'CakePHP: the rapid development php framework');
	// $cakeVersion = __d('cake_dev', 'CakePHP %s', Configure::version())
	?>
	<!DOCTYPE html>
	<html>
	<head>
		<?php echo $this->Html->charset(); ?>
		<title>
			<?php echo 'Recipe Finder: O lugar certo para você procurar a receita perfeita!' ?>
			<?php //echo $this->fetch('title'); ?>
		</title>
		<?php
			echo $this->Html->meta('icon');

			//echo $this->Html->css('cake.generic');

			echo $this->fetch('meta');

			echo $this->Html->css('/tema/css/bootstrap.min');
			echo $this->Html->css('/tema/font-awesome/4.3.0/css/font-awesome.min');
			echo $this->Html->css('/tema/css/icon');
			echo $this->Html->css('/tema/css/loader');
			echo $this->Html->css('/tema/css/idangerous.swiper');
			echo $this->Html->css('/tema/css/jquery-ui');
			//echo $this->Html->css('tema/css/magnific');
			echo $this->Html->css('/tema/css/stylesheet');
			echo $this->Html->script('/tema/js/jquery-2.1.4.min');
			echo $this->Html->script('/tema/js/jquery-ui.min');
			echo $this->Html->script('/tema/js/jquery.mixitup');
			echo $this->Html->script('/tema/js/jquery.viewportchecker.min');


			//echo $this->Html->css('tema/css/loader');


			echo $this->fetch('css');
		?>

		<style>
			#container {
				padding-top:50px !important;
			}
		</style>
	</head>
	<body>
		<div class="be-loader">
			<div class="spinner">
				<p class="circle">
					<span class="ouro">
						<span class="left"><span class="anim"></span></span>
						<span class="right"><span class="anim"></span></span>
					</span>
				</p>
			</div>
		</div>
		<header>

			<div class="container-fluid custom-container">
				<div class="row no_row row-header">
					<div class="brand-be">
						<?= $this->Html->link(
							'<h2><span class="label label-default">R.</span></h2>',
							array('controller' => 'Home', 'action' => 'index'),
							array('escape' => false)
						); ?>
						<!-- <a href="/">
							<h2><span class="label label-default">R.</span></h2>
						</a> -->
					</div>
					<div class="header-menu-block">
						<button class="cmn-toggle-switch cmn-toggle-switch__htx"><span></span></button>
						<ul class="header-menu" id="one">
							<li>
								<?=
								$this->Html->link(
									'Nova Pesquisa',
									array('controller' => 'Home', 'action' => 'index'));
								?>
							</li>
							<li><a href="#">Usuários</a>
								<ul>
									<li>
											<?=
											$this->Html->link(
												'Listar',
												array('controller' => 'Usuarios', 'action' => 'index'));
											?>
										</a>
									</li>
									<li>
											<?=
											$this->Html->link(
												'Cadastrar',
												array('controller' => 'Usuarios', 'action' => 'cadastrar'));
											?>
										</a>
									</li>
								</ul>
							</li>
							<li><a href="#">Ingredientes / Receitas</a>
								<ul>
									<li>
											<?=
											$this->Html->link(
												'Cadastrar Ingrediente',
												array('controller' => 'Ingredientes', 'action' => 'cadastrar'));
											?>
										</a>
									</li>
									<li>
											<?=
											$this->Html->link(
												'Get',
												array('controller' => 'Ingredientes', 'action' => 'index'));
											?>
										</a>
									</li>
									<li>
											<?=
											$this->Html->link(
												'Get Ingredient For Recipe',
												array('controller' => 'Ingredientes', 'action' => 'IngredientesReceitas/R3c1p3F1nd3R13asd13asd'));
											?>
										</a>
									</li>
								</ul>
							</li>
							<li><a href="#">Olá mundo</a>
								<ul>
									<li><a href="search.html">Explore</a></li>
									<li><a href="people.html">People</a></li>
									<li><a href="gallery.html">Galleries</a></li>
								</ul>
							</li>
						</ul>
					</div>
					<?php $usuarioSessao = $this->Session->read('User'); ?>
					<?php if(is_null($usuarioSessao)):?>
						<div class="login-header-block">
							<div class="login_block">
								<a class="btn-login btn color-1 size-2 hover-2" href="#" ><i class="fa fa-user"></i>
								Entrar</a>
							</div>
						</div>
					</div>
				<?php else: ?>
					<div class="login-header-block">
						<div class="login_block">
							<?=
							$this->Html->link(
								'<i class="fa fa-sign-out"></i>Sair',
								array('controller' => 'Users', 'action' => 'logout'), array('class' => 'btn', 'escape' => false));
							?>
							<!-- <a class="btn" href="/users/logout" ><i class="fa fa-sign-out"></i>
								Log out</a> -->
								<?php echo
								$this->Html->link(
								'<i class="fa fa-user"></i>Bem vindo, ' . $usuarioSessao['nome'],
								array('controller' => 'Usuarios', 'action' => 'visualizar'), array('class' => 'btn', 'escape' => false)
							);
							 ?>
							<!-- <a class="btn" href="/users/page/<?php echo $usuarioSessao["id"] ?>" ><i class="fa fa-user"></i>
							Bem vindo,	<?php //echo $usuarioSessao["nome"] ?></a> -->
						</div>
					</div>
				<?php endif; ?>
				</div>
			</div>
		</header>

		<div id="content-block">
			<?php if(is_null($usuarioSessao)):?>
			<div class="head-bg">
				<div class="head-bg-img"></div>
				<div class="head-bg-content">
					<h1>Receitas Culinárias</h1>
					<p>Recipe Finder: O lugar certo para você procurar a receita perfeita!</p>
					<a class="btn color-1 size-1 hover-1" ><i class="fa fa-facebook"></i>Entrar com Facebook</a>
					<a class="be-register btn color-3 size-1 hover-6"><i class="fa fa-lock"></i>Cadastre-se já</a>
				</div>
			</div>
		<?php endif; ?>
		</div>

		<div class="large-popup login">
			<div class="large-popup-fixed"></div>
			<div class="container large-popup-container">
				<div class="row">
					<div class="col-md-8 col-md-push-2 col-lg-6 col-lg-push-3  large-popup-content">
						<div class="row">
							<div class="col-md-12">
								<i class="fa fa-times close-button"></i>
								<h5 class="large-popup-title">Entrar</h5>
							</div>
							<div class="col-md-12">
								<h2 id="resposta_login"></h2>
							</div>
							<?php echo $this->Form->create('User', array('id' => 'login_usuario')) ?>
							<!-- <form action="" id="login_usuario" type="POST" class="popup-input-search"> -->
							<div class="col-md-6">
								<?php echo $this->Form->input('email', array('class' => 'input-signtype', 'placeholder' => 'Seu email', 'required' => '', 'label' => false)) ?>
								<!-- <input class="input-signtype" name="email" type="email" required="" placeholder="Seu email"> -->
							</div>
							<div class="col-md-6">
								<?php echo $this->Form->input('senha', array('class' => 'input-signtype', 'placeholder' => 'Senha', 'required' => '', 'type' => 'password', 'label' => false)) ?>
								<!-- <input class="input-signtype" name="senha" type="password" required="" placeholder="Senha"> -->
							</div>
							<div class="col-xs-6">
								<?=
								$this->Html->link(
									'Cadastrar Ingrediente',
									array('controller' => 'Users', 'action' => 'recover'));
								?>
								<!-- <a href="/users/recover" class="link-large-popup">Recuperar Senha?</a> -->
							</div>
							<div class="col-xs-6 for-signin">
								<input type="submit" class="be-popup-sign-button" value="SIGN IN">
								<?php echo $this->Form->end() ?>
							</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="large-popup register">
			<div class="large-popup-fixed"></div>
			<div class="container large-popup-container">
				<div class="row">
					<div class="col-md-10 col-md-push-1 col-lg-8 col-lg-push-2 large-popup-content">
						<div class="row">
							<div class="col-md-12">
								<i class="fa fa-times close-button"></i>
								<h5 class="large-popup-title">Cadastrar</h5>
							</div>
							<?php echo $this->Form->create('User', array('id' => 'cadastro_usuario')) ?>
							<!-- <form action="/users/create" class="popup-input-search" action="POST" id="cadastro_usuario"> -->
								<div class="col-md-6">
									<?php echo $this->Form->input('nome', array('class' => 'input-signtype', 'placeholder' => 'Nome', 'required' => '', 'label' => false)) ?>
									<?php echo $this->Form->hidden('tipo_usuario', array('value' => '2')); ?>
									<!-- <input class="input-signtype" type="text" required="" placeholder="Nome" name="nome"> -->
								</div>
								<div class="col-md-6">
									<?php echo $this->Form->input('email', array('class' => 'input-signtype', 'placeholder' => 'E-mail', 'required' => '', 'label' => false)) ?>
									<!-- <input class="input-signtype" type="email" required="" placeholder="Email" name="email"> -->
								</div>
								<div class="col-md-6">
									<?php echo $this->Form->input('senha', array('class' => 'input-signtype', 'placeholder' => 'Senha', 'required' => '', 'type' => 'password', 'label' => false)) ?>
									<!-- <input class="input-signtype" type="password" required="" placeholder="Senha" name="senha"> -->
								</div>
								<div class="col-md-6">
									<?php echo $this->Form->input('senha_confirmacao', array('class' => 'input-signtype', 'placeholder' => 'Repita sua senha', 'required' => '', 'type' => 'password', 'label' => false)) ?>
									<!-- <input class="input-signtype" type="password" required="" placeholder="Repita sua senha" name="senha_confirmacao" > -->
								</div>
								<div class="col-md-6 for-signin">
									<input type="submit" class="be-popup-sign-button" value="CADASTRAR">
									<?php echo $this->Form->end() ?>
								</div>
							<!-- </form> -->
							<div><p id="resposta_cadastro"></p></div>
						</div>
					</div>
				</div>
			</div>
		</div>


			<div id="container">
				<div id="content">

					<?php echo $this->Flash->render(); ?>

					<?php echo $this->fetch('content'); ?>
				</div>
				<div id="footer">
					<?php /*echo $this->Html->link(
							$this->Html->image('cake.power.gif', array('alt' => $cakeDescription, 'border' => '0')),
							'http://www.cakephp.org/',
							array('target' => '_blank', 'escape' => false, 'id' => 'cake-powered')
						);*/
					?>
					<p>
						<?php //echo $cakeVersion; ?>
					</p>
				</div>
			</div>
		<?php //echo $this->element('sql_dump'); ?>
		<!-- <div class="theme-config">
		    <div class="main-color">
		        <div class="title">Main Color:</div>
		        <div class="colours-wrapper">
		            <div class="entry color1 m-color active" data-colour="/tema/css/stylesheet.css"></div>
		            <div class="entry color3 m-color"  data-colour="/tema/css/style-green.html"></div>
		            <div class="entry color6 m-color"  data-colour="/tema/css/style-orange.html"></div>
		            <div class="entry color8 m-color"  data-colour="/tema/css/style-red.html"></div>
		            <div class="title">Second Color:</div>
		            <div class="entry s-color  active color10"  data-colour="/tema/css/stylesheet.css"></div>
		            <div class="entry s-color color11"  data-colour="/tema/css/style-oranges.html"></div>
		            <div class="entry s-color color12"  data-colour="/tema/css/style-greens.html"></div>
		            <div class="entry s-color color13"  data-colour="/tema/css/style-reds.html"></div>
		        </div>
		    </div>
		   <div class="open"><?= $this->Html->image('icon-134.png'); ?></div>
		</div> -->
	</body>
	<?php
		echo $this->Html->script('/tema/js/bootstrap.min');
		echo $this->Html->script('/tema/js/idangerous.swiper.min');
		echo $this->Html->script('/tema/js/filters');
		echo $this->Html->script('/tema/js/global');
		echo $this->fetch('script');

	?>
	<script type="text/javascript">
	var myBaseUrl = '<?php echo $this->html->url('/', true); ?>';
	$(document).click(function(event) {
	    if(!$(event.target).closest('.be-drop-down').length &&
	       !$(event.target).is('.be-drop-down')) {
	        if($('.be-drop-down').hasClass("be-dropdown-active")) {
	            $('.be-drop-down').removeClass("be-dropdown-active");
	        }
	    }
	})
	$('#cadastro_usuario').submit(function(event){
		event.preventDefault();
		dados = new FormData(this);
		$.ajax({
			type: 'POST',
			url: myBaseUrl + "/Users/Create",
			data: dados,
			processData: false,
			cache: false,
			contentType: false,
			dataType: "json",
			success: function(json) {
				if('sucesso' in json) {
					$("#resposta_cadastro").html(json.sucesso);
					setTimeout(function(){location.reload()},2000);

					$("#resposta_cadastro").removeClass("text-danger");
					$("#resposta_cadastro").addClass("text-success");
				} else {
					$("#resposta_cadastro").html(json.erro);
					$("#resposta_cadastro").addClass("text-danger");

				}
			}
		});
	});

	$('#login_usuario').submit(function(event){
		event.preventDefault();
		dados = new FormData(this);
		$.ajax({
			type: 'POST',
			url: myBaseUrl + "/Users/login",
			data: dados,
			processData: false,
			cache: false,
			contentType: false,
			dataType: "json",
			success: function(json) {
				if('sucesso' in json) {
					$("#resposta_login").html(json.sucesso);
					setTimeout(function(){location.reload()},1000);

					$("#resposta_login").removeClass("text-danger");
					$("#resposta_login").addClass("text-success");
				} else {
					$("#resposta_login").html(json.erro);
					$("#resposta_login").addClass("text-danger");

				}
			}
		});
	});
	</script>

	</html>
