<?php echo $this->Form->create('CategoryRecipe', array('id' => 'cadastro_categoria_receita')) ?>
<div class="sec">
  <div class="be-large-post">
    <div class="info-block style-2">
      <div class="be-large-post-align"><h3 class="info-block-label">Cadastro categoria receita</h3></div>
    </div>
    <div id="resposta_cadastro"></div>
    <div class="be-large-post-align">
      <div class="row">
        <div class="input-col col-xs-12 col-sm-4">
          <div class="form-group focus-2">
            <div class="form-label">Nome da Categoria da Receita</div>
          </div>
          <?= $this->Form->input('nome', array('class' => 'form-input', 'placeholder' => 'Nome Categoria', 'required' => '', 'label' => false)) ?>
        </div>
        <div class="input-col col-xs-12 col-sm-4">
          <div class="form-group focus-2">
            <div class="form-label">Status</div>
            <?php $ops = array('1'=>'Ativo','0'=>'Inativo')?>
            <?= $this->Form->radio('status', $ops, array('legend' => false, 'label' => false)); ?>
          </div>
        </div>
        <div class="col-xs-12">
          <input class="btn color-1 size-2 hover-1 btn-right" type="submit" value="Salvar">
        </div>
      </div>
    </div>
  </div>
</div>
<?php echo $this->Form->end() ?>

<script type="text/javascript">
$('#cadastro_categoria_receita').submit(function(event){
  event.preventDefault();
  dados = new FormData(this);
  $.ajax({
    type: 'POST',
    url: myBaseUrl + "/CategoryRecipe/Create",
    data: dados,
    processData: false,
    cache: false,
    contentType: false,
    dataType: "json",
    success: function(json) {
      if('sucesso' in json) {
        $("#resposta_cadastro").html(json.sucesso);

        $("#resposta_cadastro").removeClass("text-danger");
        $("#resposta_cadastro").addClass("text-success");
      } else {
        $("#resposta_cadastro").html(json.erro);
        $("#resposta_cadastro").removeClass("text-success");
        $("#resposta_cadastro").addClass("text-danger");

      }
    }
  });
});
</script>
