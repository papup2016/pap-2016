  <div class="sec">
    <div class="be-large-post">
      <div class="info-block style-2">
        <div class="be-large-post-align"><h3 class="info-block-label">Cadastro categoria receita</h3></div>
      </div>
      <div id="resposta_cadastro"></div>
      <div class="be-large-post-align">
        <table>
          <?php foreach ($CategoryRecipe as $key => $value) : ?>
            <tr>
              <td>
                <?= $value["CategoryRecipe"]["nome"] ?>
              </td>
              <td>
                <?= ($value["CategoryRecipe"]["status"])?"Ativo":"Inativo"; ?>
              </td>
              <td>
                <?=
                $this->Html->link(
                'Editar',
                array('controller' => 'CategoryRecipe', 'action' => 'edit',$value["CategoryRecipe"]["id"]));
                ?>
              </td>
            </tr>
          <?php endforeach; ?>
        </table>
      </div>
    </div>
  </div>
