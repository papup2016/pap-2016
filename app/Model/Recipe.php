<?php
App::uses('Model', 'Model');
class Recipe extends Model {
  public $useTable = "recipes";
  public $hasAndBelongsToMany = array (
            'Categoria' => array (
                    'className' => 'CategoryRecipe',
                    'joinTable' => 'categorias_receitas',
                    'foreignKey' => 'receitas_id',
                    'associationForeignKey' => 'categoria_receita_id'
            )
    );
}
