<?php
App::uses('Model', 'Model');
class CategoryRecipe extends Model {
  public $useTable = "categoria_receita";
  public $hasAndBelongsToMany = array (
            'Receita' => array (
                    'className' => 'Recipe',
                    'joinTable' => 'categorias_receitas',
                    'foreignKey' => 'categoria_receita_id',
                    'associationForeignKey' => 'receitas_id'
            )
    );
}
