-- MySQL dump 10.13  Distrib 5.6.24, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: recipefinder
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `categoria_ingrediente`
--

DROP TABLE IF EXISTS `categoria_ingrediente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_ingrediente` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_ingrediente`
--

LOCK TABLES `categoria_ingrediente` WRITE;
/*!40000 ALTER TABLE `categoria_ingrediente` DISABLE KEYS */;
INSERT INTO `categoria_ingrediente` VALUES (1,'Vegetal'),(2,'Salada'),(3,'Fruta'),(5,'Tempero');
/*!40000 ALTER TABLE `categoria_ingrediente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categoria_receita`
--

DROP TABLE IF EXISTS `categoria_receita`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categoria_receita` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) DEFAULT NULL,
  `status` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categoria_receita`
--

LOCK TABLES `categoria_receita` WRITE;
/*!40000 ALTER TABLE `categoria_receita` DISABLE KEYS */;
/*!40000 ALTER TABLE `categoria_receita` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorias_receitas`
--

DROP TABLE IF EXISTS `categorias_receitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorias_receitas` (
  `receitas_id` int(11) NOT NULL,
  `categoria_receita_id` int(11) NOT NULL,
  PRIMARY KEY (`receitas_id`,`categoria_receita_id`),
  KEY `fk_receitas_has_categoria_receita_categoria_receita1_idx` (`categoria_receita_id`),
  KEY `fk_receitas_has_categoria_receita_receitas1_idx` (`receitas_id`),
  CONSTRAINT `fk_receitas_has_categoria_receita_categoria_receita1` FOREIGN KEY (`categoria_receita_id`) REFERENCES `categoria_receita` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_receitas_has_categoria_receita_receitas1` FOREIGN KEY (`receitas_id`) REFERENCES `receitas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorias_receitas`
--

LOCK TABLES `categorias_receitas` WRITE;
/*!40000 ALTER TABLE `categorias_receitas` DISABLE KEYS */;
/*!40000 ALTER TABLE `categorias_receitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favoritas`
--

DROP TABLE IF EXISTS `favoritas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `favoritas` (
  `receitas_id` int(11) NOT NULL,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`receitas_id`),
  KEY `fk_receitas_has_usuarios_receitas1_idx` (`receitas_id`),
  KEY `fk_favoritas_users1_idx` (`users_id`),
  CONSTRAINT `fk_favoritas_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_receitas_has_usuarios_receitas1` FOREIGN KEY (`receitas_id`) REFERENCES `receitas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favoritas`
--

LOCK TABLES `favoritas` WRITE;
/*!40000 ALTER TABLE `favoritas` DISABLE KEYS */;
/*!40000 ALTER TABLE `favoritas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredientes`
--

DROP TABLE IF EXISTS `ingredientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredientes` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(60) DEFAULT NULL,
  `categoria_ingrediente_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_ingredientes_categoria_ingrediente1_idx` (`categoria_ingrediente_id`),
  CONSTRAINT `fk_ingredientes_categoria_ingrediente1` FOREIGN KEY (`categoria_ingrediente_id`) REFERENCES `categoria_ingrediente` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=673 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredientes`
--

LOCK TABLES `ingredientes` WRITE;
/*!40000 ALTER TABLE `ingredientes` DISABLE KEYS */;
INSERT INTO `ingredientes` VALUES (1,'teste',3),(3,'xeupis',3),(4,'bingo',3),(5,'teta',5),(6,'BrÃ³colis',1),(666,'xalaba',5),(668,'toba',2),(669,'tetas',5),(670,'brÃ³colis2',1),(672,'olÃ¡',1);
/*!40000 ALTER TABLE `ingredientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ingredientes_receitas`
--

DROP TABLE IF EXISTS `ingredientes_receitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ingredientes_receitas` (
  `ingredientes_id` int(11) NOT NULL,
  `receitas_id` int(11) NOT NULL,
  PRIMARY KEY (`ingredientes_id`,`receitas_id`),
  KEY `fk_ingredientes_has_receitas_receitas1_idx` (`receitas_id`),
  KEY `fk_ingredientes_has_receitas_ingredientes1_idx` (`ingredientes_id`),
  CONSTRAINT `fk_ingredientes_has_receitas_ingredientes1` FOREIGN KEY (`ingredientes_id`) REFERENCES `ingredientes` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_ingredientes_has_receitas_receitas1` FOREIGN KEY (`receitas_id`) REFERENCES `receitas` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ingredientes_receitas`
--

LOCK TABLES `ingredientes_receitas` WRITE;
/*!40000 ALTER TABLE `ingredientes_receitas` DISABLE KEYS */;
/*!40000 ALTER TABLE `ingredientes_receitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `receitas`
--

DROP TABLE IF EXISTS `receitas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `receitas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) DEFAULT NULL,
  `descricao` longtext,
  `users_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_receitas_users1_idx` (`users_id`),
  CONSTRAINT `fk_receitas_users1` FOREIGN KEY (`users_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `receitas`
--

LOCK TABLES `receitas` WRITE;
/*!40000 ALTER TABLE `receitas` DISABLE KEYS */;
/*!40000 ALTER TABLE `receitas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_usuario`
--

DROP TABLE IF EXISTS `tipo_usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_usuario`
--

LOCK TABLES `tipo_usuario` WRITE;
/*!40000 ALTER TABLE `tipo_usuario` DISABLE KEYS */;
INSERT INTO `tipo_usuario` VALUES (1,'Administrador'),(2,'Usuário');
/*!40000 ALTER TABLE `tipo_usuario` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `localizacao` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `uid_facebook` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_facebook` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `link_facebook` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `senha` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `login` varchar(90) COLLATE utf8_unicode_ci DEFAULT NULL,
  `tipo_usuario` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_users_tipo_usuario1_idx` (`tipo_usuario`)
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'usuario',NULL,NULL,NULL,NULL,'usuario@usuario.com','TVRNM056RTFNREE9',NULL,1),(2,'Julyano',NULL,NULL,NULL,NULL,'julykramer@hotmail.com','TVRNM056RTFNREE9',NULL,2);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'recipefinder'
--

--
-- Dumping routines for database 'recipefinder'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-05-19 17:10:16
